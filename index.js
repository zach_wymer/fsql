// Functional sql generator:
// fsql()
// select('columna', 'columnb', 'columnc')
// .from('table t1')
// .innerJoin('table2 t2')
//      .on('t1.columna').equals('t2.columnb')
// .toList()

// fsql()
// .select('*')
// .from('Logins')
// .where('userID')
// .in()
//     .select('id')
//     .from('users').where('id').equals(1)
// .closeParen()
function fsql() {
    const self = this;
    self.sql_stack = [];
    this.select = (...columns) => {
        this.sql_stack.push({type: 'select', columns})
        return  {
            from: self.from.bind(self),
            where: self.where.bind(self),
            toString: self.toString.bind(self)
        }
    }
    this.from = (table) => {
        this.sql_stack.push({type: 'from', table})
        return  {
            where: self.where.bind(this),
            toList: self.toList.bind(this),
            ...joinable,
            ...standard
        };
    }
    
    this.in = (clause) => {
        this.sql_stack.push(
                {type: 'in', clause: clause}
            )
        return {
            toList: self.toList.bind(this),
            toString: self.toString.bind(this),
            select: self.select.bind(this),
            ...logicOperators,
            ...standard
        }
    }

    this.where = (clause) => {
        this.sql_stack.push({type: 'where', clause})
        return {
            innerJoin: self.innerJoin.bind(this),
            toList: self.toList.bind(this),
            select: self.select.bind(this),
            orderBy: self.orderBy.bind(this),   
            ...logicOperators,
            ...standard
        }
    }

    this.exists = (clause) => {
        this.sql_stack.push({type: 'exists', clause});
        return {
            select: self.select.bind(this)
        }
    }

    this.openParen = () => {
        this.sql_stack.push({type: 'openParen'})
        return {
            column: self.column.bind(this),
            select: self.select.bind(this)
        }
    }
    this.closeParen = () => {
        this.sql_stack.push({type: 'closeParen'})
        return {
            select: self.select.bind(this),
            ...logicOperators,
            ...standard
        }
    }

    this.groupBy = (...columns) => {
        this.sql_stack.push({type: 'groupBy', columns});
        return {
            orderBy: self.orderBy.bind(this),
            ...logicOperators,
            ...standard
        }
    }

    this.and = (value) => {
        this.sql_stack.push({type: 'and', value});
        return {
            ...logicOperators,
            ...standard
        }
    }

    this.or = (column) => {
        this.sql_stack.push({type: 'or', column})
        return {
            ...logicOperators,
            ...standard
        }
    }

    this.innerJoin = (table) => {
        this.sql_stack.push({type: 'innerJoin', table});
        return {
            on: self.on.bind(this),
            toList: self.toList.bind(this),
            ...standard
        }
    }

    this.leftJoin = (table) => {
        this.sql_stack.push({type: 'leftJoin', table})
        return {
            on: self.on.bind(this),
            toList: self.toList.bind(this),
            ...standard
        }
    }

    this.on = (column) => {
        this.sql_stack.push({type: 'on', column});
        return {
            ...logicOperators,
            ...standard
        }
    }

    this.equals = (value) => {
        this.sql_stack.push({type: 'equals', value})
        return {
            toList:  self.toList.bind(this),
            groupBy: self.groupBy.bind(this),
            where: self.where.bind(this),
            select: self.select.bind(this),
            ...joinable,
            ...logicOperators,
            ...standard
        }
    }

    this.with = (table) => {
        this.sql_stack.push({type: 'with', table})
        return {
            select: self.select.bind(this),
            ...standard
        }
    }

    this.column = (column) => {
        this.sql_stack.push({type: 'column', column});
        return {
            ...logicOperators,
            ...standard
        }
    }
    
    this.notEquals = (value) => {
        this.sql_stack.push({type: 'notEquals', value});
        return {
            ...standard
        }
    }

    this.orderBy = (...columns) => {
        this.sql_stack.push({type: 'orderBy', columns});
        return {
            ...standard
        }
    }

    this.findSql = (search) => {
        return this.sql_stack.find(search);
    }

    this.toList = () => {
        return sql_stack.join(' ');
    }

    this.toString = () => {
        let sql_str = null;
        let indent_lvl = 0;
        let lastType = null;
        let indentEnding = false;
        const column = (column) => `[${column}]`;
        const columns = (sql) => `${sql.columns.map(col => column(col)).join(', ')}`;
        return sql_stack.map((sql, i, stack) => {
            switch(sql.type) {
                case 'where': sql_str =  `where ${sql.clause}`; break;
                case 'from': sql_str = `from ${sql.table}`; break;
                case 'select': sql_str = `select ${columns(sql)}`; break;
                case 'equals': sql_str = `= ${sql.value}`; break;
                case 'innerJoin': sql_str = `inner join ${sql.table}`; break;
                case 'on': sql_str = `on ${sql.column}`; break;
                case 'groupBy': sql_str = `group by ${columns(sql)}`; break;
                case 'or': sql_str = `or ${sql.column}`; break;
                case 'between': sql_str = `between ${sql.value}`; break;
                case 'and': sql_str = `and ${sql.value}`; break;
                case 'in': sql_str = `in (${sql.clause ? `${sql.clause})` : ''}`; break;
                case 'notIn': sql_str = `not in (${t}${sql.clause})`; break;
                case 'notEquals': sql_str = `<> ${sql.value}`; break;
                case 'openParen': sql_str = $`(`; break;
                case 'closeParen': sql_str = `)`; break;
                case 'column': sql_str = `${column(sql.column)}`; break;
                case 'with': sql_str = `with ${sql.table} as (`; break;
                case 'orderBy': sql_str = `order by ${columns(sql)}`; break;
                case 'leftJoin': sql_str = `left join ${sql.table}`; break;
                
                default: throw `${sql.type} is not defined. Please hit up the developer.`
            }
            let formatting = undefined;
            const indent = (count) => count > 0 ?  ' '.repeat(count * 4) : ''
            switch(sql.type) {
                case 'on': formatting = `\n${indent(++indent_lvl)}`; break;
                case 'in': 
                case 'with':
                    formatting = '';
                    indent_lvl++;
                    indentEnding = false;
                    break;
                case 'on':
                case 'equals':
                    break;
                case 'select':
                case 'from':
                case 'and':
                    indentEnding = false;
                    formatting = `\n${indent(indent_lvl)}`;
                    break;
                case 'where':
                    formatting = `\n${indent(!indentEnding ? indent_lvl : --indent_lvl)}`
                    break;
                case 'closeParen':
                    formatting = `\n${indent(--indent_lvl)}`;  
                    break;
                default: 
                    formatting = `\n${indent(indent_lvl)}`
                    indentEnding = true;
            }
            if ( ['leftJoin', 'innerJoin', 'where'].includes(sql.type)  &&  
                 ['on', 'equals', 'and', 'or', 'notEquals'].some(comp => comp === lastType)) {
                formatting = `\n${indent(--indent_lvl)}`;
            }
            
            lastType = sql.type;
            return `${formatting ? formatting : ''}${sql_str}`;
        }).join(' ')
    }
    const joinable = {
        leftJoin: self.leftJoin.bind(this),
        innerJoin: self.innerJoin.bind(this)
    }
    const logicOperators = {
        and: self.and.bind(this),
        or: self.or.bind(this),
        openParen: self.openParen.bind(this),
        closeParen: self.closeParen.bind(this),
        in: self.in.bind(this),
        equals: self.equals.bind(this),
        notEquals: self.notEquals.bind(this),
        exists: self.exists.bind(this),
    }
    const standard = {
        toString: self.toString.bind(this)
    }
    return this;
}
document.writeln(
    `<pre>${
     fsql()
     .with('administrators')
        .select('UserID')
        .from('Administrators')
        .where('deleted').equals('0')
    .closeParen()
    .select('FirstName', 'LastName')
    .from('Users u')
    .innerJoin('Permissions p')
         .on('u.UserID').equals('p.UserID')
         .and('deleted').equals('false')
    .innerJoin('administrators a')
        .on('a.UserID').equals('u.UserID')
        .and('deleted').equals('false')
    .leftJoin('Ratings r')
        .on('r.userID').equals('u.userID')
    .where('userID')
        .in()
            .select('userID')
            .from('BannedUsers')
            .where('userID').equals('u.userID')
        .closeParen()
        .and('deleted').equals(false)
    .groupBy('FirstName', 'LastName')
    .orderBy('LastName')
    }</pre>`    
)